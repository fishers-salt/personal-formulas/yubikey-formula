# frozen_string_literal: true

control 'yubikey-service-clean' do
  title 'should be stopped and disabled'

  describe service('pcscd.socket') do
    it { should_not be_enabled }
    it { should_not be_running }
  end
end
