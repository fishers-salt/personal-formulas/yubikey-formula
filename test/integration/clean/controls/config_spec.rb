# frozen_string_literal: true

control 'yubikey-config-file-udev-rules-absent' do
  title 'should not exist'

  describe file('/etc/udev/rules.d/71-gnupg-ccid.rules') do
    it { should_not exist }
  end
end

control 'yubikey-config-file-zsh-rc-includes-config-auser-absent' do
  title 'should not exist'

  describe file('/home/auser/.config/zsh/rc_includes/yubikey.zsh') do
    it { should_not exist }
  end
end

control 'yubikey-config-file-yubikey-key-auser-2404C9546E145360-removed' do
  title 'key should not be in keyring'

  describe command(
    'gpg --homedir "/home/auser/.gnupg" --list-keys | grep 2404C9546E145360'
  ) do
    its('stdout') { should eq '' }
    its('exit_status') { should eq 1 }
  end
end
