# frozen_string_literal: true

control 'yubikey-config-file-udev-rules-managed' do
  title 'should match desired lines'

  describe file('/etc/udev/rules.d/71-gnupg-ccid.rules') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('ACTION=="add",') }
  end
end

control 'yubikey-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'yubikey-config-file-zsh-rc-include-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/rc_includes') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'yubikey-config-file-zsh-rc-includes-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/rc_includes/yubikey.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') do
      should include('SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)')
    end
  end
end

control 'yubikey-config-file-gpg-agent-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.gnupg/gpg-agent.conf') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0600' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') { should include('pinentry-program /usr/bin/pinentry-gnome3') }
  end
end

control 'yubikey-config-file-yubikey-key-auser-2404C9546E145360-added' do
  title 'key should be added'

  describe command(
    'gpg --homedir "/home/auser/.gnupg" --list-keys | grep 2404C9546E145360'
  ) do
    its('stdout') { should match '2404C9546E145360' }
    its('exit_status') { should eq 0 }
  end
end
