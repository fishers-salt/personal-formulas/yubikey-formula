# frozen_string_literal: true

control 'yubikey-service-running' do
  title 'should be running and enabled'

  describe service('pcscd.socket') do
    it { should be_enabled }
    it { should be_running }
  end
end
