# frozen_string_literal: true

packages =
  case os[:name]
  when 'arch'
    %w[gnupg
       pcsclite
       ccid
       hopenpgp-tools
       yubikey-personalization]
  else
    %w[gnupg2
       gnupg-agent
       dirmngr
       cryptsetup
       scdaemon
       pcscd
       hopenpgp-tools
       yubikey-personalization]
  end

packages.each do |pkg|
  control "yubikey-package-install-pkg-#{pkg}-installed" do
    title "#{pkg} should be installed on #{os[:name]}"

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
