# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as yubikey with context %}

yubikey-service-clean:
  service.dead:
    - name: {{ yubikey.service.name }}
    - enable: False
