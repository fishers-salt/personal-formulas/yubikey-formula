# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as yubikey with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

yubikey-config-file-udev-rules-managed:
  file.managed:
    - name: /etc/udev/rules.d/71-gnupg-ccid.rules
    - source: {{ files_switch(['gnupg-ccid.rules.tmpl'],
                              lookup='yubikey-config-file-udev-rules-managed'
                             )
              }}
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja

{% if salt['pillar.get']('yubikey-formula:use_users_formula', False) %}
{%- set keyserver = yubikey['keyserver']  %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_yubikey', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

yubikey-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

yubikey-config-file-gpg-home-{{ name }}-created:
  cmd.run:
    - name: gpg --homedir "{{ home }}/.gnupg" --list-keys
    - runas: {{ name }}
    - creates: {{ home }}/.gnupg/pubring.kbx

yubikey-config-file-gpg-home-ownership-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.gnupg
    - user: {{ name }}
    - group: {{ user_group }}
    - file_mode: '0600'
    - dir_mode: '0700'
    - recurse:
      - user
      - group
      - mode
    - require:
      - yubikey-config-file-gpg-home-{{ name }}-created

yubikey-config-file-zsh-rc-include-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/zsh/rc_includes
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - yubikey-config-file-user-{{ name }}-present

yubikey-config-file-zsh-rc-includes-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/zsh/rc_includes/yubikey.zsh
    - source: {{ files_switch([
                  name ~ '-yubikey_rc.zsh.tmpl',
                  'yubikey_rc.zsh.tmpl'],
                lookup='yubikey-config-file-zsh-rc-includes-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - yubikey-config-file-zsh-rc-include-dir-{{ name }}-managed

yubikey-config-file-gpg-agent-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.gnupg/gpg-agent.conf
    - source: {{ files_switch([
                  name ~ '-gpg-agent.conf.tmpl',
                  'gpg-agent.conf.tmpl'],
                lookup='yubikey-config-file-gpg-agent-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0600'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - yubikey-config-file-gpg-home-ownership-{{ name }}-managed

{% set trusted_keys = user.get('yubikey_trusted_keys', []) %}
{% for key in trusted_keys %}
{# This still doesn't seem to work :( #}
{#
yubikey-config-file-yubikey-key-{{ name }}-{{ key }}-added:
  gpg.present:
    - name: {{ key }}
    - keyserver: hkps://keys.openpgp.org
    - user: {{ name }}
    - trust: ultimately
    - gnupghome: {{ home }}/.gnupg
    - require:
      - yubikey-config-file-user-{{ name }}-present
      - sls: {{ sls_package_install }}
      - yubikey-config-file-gpg-home-{{ name }}-created
      - yubikey-config-file-gpg-home-ownership-{{ name }}-managed
      #}
{# Use this instead #}
yubikey-config-file-yubikey-key-{{ name }}-{{ key }}-manually-added:
  cmd.run:
    - name: gpg --keyserver {{ keyserver }} --recv {{ key }}
    - runas: {{ name }}
    - unless: gpg --list-keys | grep {{ key }}

yubikey-config-file-yubikey-key-{{ name }}-{{ key }}-manually-trusted:
  cmd.run:
    - name: echo -e "5\ny\n" |  gpg --command-fd 0 --expert --no-tty --edit-key {{ key }} trust
    - runas: {{ name }}
    - onchanges:
      - cmd: yubikey-config-file-yubikey-key-{{ name }}-{{ key }}-manually-added

{% endfor %}

{% endif %}
{% endfor %}
{% endif %}
