# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_service_clean = tplroot ~ '.service.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as yubikey with context %}

include:
  - {{ sls_service_clean }}

yubikey-config-file-udev-rules-absent:
  file.absent:
    - name: /etc/udev/rules.d/71-gnupg-ccid.rules

{% if salt['pillar.get']('yubikey-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_yubikey', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

yubikey-config-file-zsh-rc-includes-config-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/zsh/rc_includes/yubikey.zsh

{% set trusted_keys = user.get('yubikey_trusted_keys', []) %}
{% for key in trusted_keys %}
yubikey-config-file-yubikey-key-{{ name }}-{{ key }}-manually-removed:
  cmd.run:
    - name: gpg --batch --yes --delete-keys {{ key }}
    - runas: {{ name }}
{% endfor %}

{% endif %}
{% endfor %}
{% endif %}
